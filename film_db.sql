-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 10, 2020 at 06:34 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `film_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrateur`
--

CREATE TABLE `administrateur` (
  `id_admin` int(11) NOT NULL,
  `login_admin` varchar(500) NOT NULL,
  `password_admin` varchar(500) NOT NULL,
  `email_admin` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrateur`
--

INSERT INTO `administrateur` (`id_admin`, `login_admin`, `password_admin`, `email_admin`) VALUES
(1, 'ali.med', '6391cbd27b9551ecd5cc394efd6ca882', 'alimohamedaliahmed@outlook.fr');

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id_film` int(11) NOT NULL,
  `titre_film` varchar(200) NOT NULL,
  `id_cate` int(11) NOT NULL,
  `dateSortie` date NOT NULL,
  `description` longtext NOT NULL,
  `image_film` varchar(200) NOT NULL,
  `id_admin_add` int(11) NOT NULL,
  `date_add` date NOT NULL,
  `date_delete` date DEFAULT NULL,
  `id_admin_delete` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `historique`
--

CREATE TABLE `historique` (
  `id_his` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `action_his` varchar(20) NOT NULL,
  `date_his` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historique`
--

INSERT INTO `historique` (`id_his`, `id_user`, `action_his`, `date_his`) VALUES
(1, 1, 'Connexion', '2019-10-23 10:00:22'),
(2, 1, 'Ajout type compte', '2019-10-23 10:47:27'),
(3, 1, 'Ajout utilisateur', '2019-10-23 10:47:47'),
(4, 1, 'Connexion', '2019-10-23 15:21:43'),
(5, 1, 'Connexion', '2019-10-23 18:15:07'),
(6, 1, 'Connexion', '2019-11-08 15:55:41'),
(7, 1, 'Connexion', '2019-11-09 22:07:38'),
(8, 1, 'Connexion', '2020-01-23 01:18:58'),
(9, 1, 'Ajout type compte', '2020-01-23 01:22:40'),
(10, 1, 'Connexion', '2020-01-25 16:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `type_compte`
--

CREATE TABLE `type_compte` (
  `id_type` int(11) NOT NULL,
  `nom_type` varchar(500) NOT NULL,
  `id_admin_add_type` int(11) NOT NULL,
  `date_add_type` date NOT NULL,
  `id_admin_delete_type` int(11) DEFAULT NULL,
  `date_delete_type` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_compte`
--

INSERT INTO `type_compte` (`id_type`, `nom_type`, `id_admin_add_type`, `date_add_type`, `id_admin_delete_type`, `date_delete_type`) VALUES
(1, 'simple', 1, '2019-10-23', NULL, NULL),
(2, 'Horreur', 1, '2020-01-23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id_user` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_admin_add` int(11) NOT NULL,
  `date_add` date NOT NULL,
  `id_admin_delete` int(11) DEFAULT NULL,
  `date_delete` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id_user`, `username`, `password`, `email`, `id_type`, `id_admin_add`, `date_add`, `id_admin_delete`, `date_delete`) VALUES
(1, 'test', '81dc9bdb52d04dc20036dbd8313ed055', 'test@email.fr', 1, 1, '2019-10-23', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id_film`),
  ADD KEY `id_cate` (`id_cate`),
  ADD KEY `id_admin_add` (`id_admin_add`),
  ADD KEY `id_admin_delete` (`id_admin_delete`);

--
-- Indexes for table `historique`
--
ALTER TABLE `historique`
  ADD PRIMARY KEY (`id_his`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `type_compte`
--
ALTER TABLE `type_compte`
  ADD PRIMARY KEY (`id_type`),
  ADD KEY `id_admin_add_type` (`id_admin_add_type`),
  ADD KEY `id_admin_delete_type` (`id_admin_delete_type`);

--
-- Indexes for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_admin_add` (`id_admin_add`),
  ADD KEY `id_admin_delete` (`id_admin_delete`),
  ADD KEY `id_type` (`id_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrateur`
--
ALTER TABLE `administrateur`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id_film` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `historique`
--
ALTER TABLE `historique`
  MODIFY `id_his` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `type_compte`
--
ALTER TABLE `type_compte`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `type_compte`
--
ALTER TABLE `type_compte`
  ADD CONSTRAINT `type_compte_ibfk_1` FOREIGN KEY (`id_admin_add_type`) REFERENCES `administrateur` (`id_admin`),
  ADD CONSTRAINT `type_compte_ibfk_2` FOREIGN KEY (`id_admin_delete_type`) REFERENCES `administrateur` (`id_admin`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
