<?php


class Film_model extends CI_Model
{
    public function getLastFilm()
    {
        $this->db->select("id_film, titre_film, dateSortie, image_film, c.nom_type");
        $this->db->from("film as f");
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->where("f.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $this->db->limit(4);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function getSixLastFilm(){
        $this->db->select("id_film, titre_film, dateSortie, image_film, c.nom_type");
        $this->db->from("film as f");
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->where("f.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $this->db->limit(3);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function getDetailtFilm($id){
        $this->db->select("id_film, titre_film, dateSortie, image_film, c.nom_type, description, lien_video");
        $this->db->from("film as f");
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->where("id_film ", $id);
        $this->db->where("f.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function record_count($id) {
        //$valid = '1';
        $this->db->select ( 'COUNT(*) AS `numrows`' );
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->where("id_cate", $id);
        $this->db->where("f.date_delete is null");
        $query = $this->db->get ( 'film as f' );
        return $query->row ()->numrows;
    }

    public function fetch_article($limit, $start, $id) {
        $this->db->limit($limit, $start);
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->where("id_cate", $id);
        $this->db->where("f.date_delete is null");
        $this->db->order_by('f.date_add', 'desc');
        $query = $this->db->get("film as f");



        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $data[] = $row;

            }

            return $data;

        }

        return false;

    }


    public function getFilmByCategories($id){
        $this->db->select("id_film, titre_film, dateSortie, image_film, c.nom_type, description, lien_video");
        $this->db->from("film as f");
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->where("id_cate ", $id);
        $this->db->where("f.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
