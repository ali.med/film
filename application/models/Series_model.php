<?php


class Series_model extends CI_Model
{
    public function getSixLastSeries(){
        $this->db->select("id_film, titre_film, dateSortie, image_film, c.nom_type");
        $this->db->from("series as s");
        $this->db->join("type_compte as c", "c.id_type = s.id_cate");
        $this->db->where("s.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $this->db->limit(6);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function getDetailtFilm($id){
        $this->db->select("id_film, titre_film, dateSortie, image_film, c.nom_type, description, lien_video");
        $this->db->from("film as f");
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->where("id_film ", $id);
        $this->db->where("f.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
