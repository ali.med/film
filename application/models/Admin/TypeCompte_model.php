<?php


class TypeCompte_model extends CI_Model
{
    public function getAllType()
    {
        $this->db->select('id_type, nom_type, ad.login_admin, date_add_type');
        $this->db->from('type_compte');
        $this->db->join('administrateur as ad', 'type_compte.id_admin_add_type = ad.id_admin');
        $this->db->where('date_delete_type is null');
        $query = $this->db->get();
        return $query->result();
    }
    public function suppressionTypeCompte($id, $data)
    {
        $this->db->where('id_type', $id );
        $this->db->update('type_compte', $data);
        return true;
    }
    public function addTypeCompte($data)
    {
        $this->db->insert('type_compte', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
}