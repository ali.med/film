<?php


class Film_model extends CI_Model
{
    public function addFilm($data)
    {
        $this->db->insert('film', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function getAllFilm(){
        $this->db->select("id_film, titre_film, nom_type, dateSortie, date_add, login_admin");
        $this->db->from("film as f");
        $this->db->join("type_compte as c", "c.id_type = f.id_cate");
        $this->db->join("administrateur as ad", "ad.id_admin = f.id_admin_add");
        $this->db->where("f.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function suppressionFilm($id, $data)
    {
        $this->db->where('id_film', $id );
        $this->db->update('film', $data);
        return true;
    }
}