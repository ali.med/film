<?php
/**
 *
 */
class Login_model extends CI_Model
{
    public function can_login($username, $password)
    {
        $this->db->select('id_admin, login_admin, email_admin');
        $this->db->from('administrateur');
        $this->db->where('login_admin', $username);
        $this->db->where('password_admin', $password);
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function log_manager($data)
    {
        $this->db->insert('historique', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
}


?>
