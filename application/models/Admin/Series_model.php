<?php


class Series_model extends CI_Model
{
    public function addSerie($data)
    {
        $this->db->insert('series', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function getAllSeries(){
        $this->db->select("id_film, titre_film, nom_type, dateSortie, date_add, login_admin");
        $this->db->from("series as s");
        $this->db->join("type_compte as c", "c.id_type = s.id_cate");
        $this->db->join("administrateur as ad", "ad.id_admin = s.id_admin_add");
        $this->db->where("s.date_delete is null");
        $this->db->order_by('id_film',"DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function suppressionSerie($id, $data)
    {
        $this->db->where('id_film', $id );
        $this->db->update('series', $data);
        return true;
    }
}