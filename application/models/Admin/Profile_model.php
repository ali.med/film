<?php


class Profile_model extends CI_Model
{
    public function addProfil($data)
    {
        $this->db->insert('profile', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getProfileUser($id)
    {
        $this->db->select('id_profile, nom_user, telephone_user, adresse_user, loaclisation_user, image_user, u.email');
        $this->db->from('profile');
        $this->db->join('utilisateurs as u', 'profile.id_user = u.id_user');
        $this->db->where('profile.id_user', $id);
        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    public function getPassword($id){
        $this->db->select('password');
        $this->db->from('utilisateurs');
        $this->db->where('id_user', $id);
        $this->db->where('date_delete is null');

        $query = $this->db->get();
        $ret = $query->row();
        return $ret->password;
    }
    public function changerMotPasse($id, $data){
        $this->db->where('id_user', $id );
        $this->db->update('utilisateurs', $data);
        return true;
    }
    public function updateProfil($id, $data)
    {
        $this->db->where('id_user', $id );
        $this->db->update('profile', $data);
        return true;
    }
    public function updatePhotoProfil($data, $id)
    {
        $this->db->where('id_user', $id );
        $this->db->update('profile', $data);
        return true;
    }
}