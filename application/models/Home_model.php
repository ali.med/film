<?php


class Home_model extends CI_Model
{
    public function getAllCategories(){
        $this->db->select("id_type, nom_type");
        $this->db->from("type_compte");
        $this->db->where('date_delete_type is null');
        $this->db->order_by('nom_type',"ASC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
