<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Ajout d'une série</div>
                        <?php if ($this->session->flashdata('error')) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Admin/Series/addSerie" method="post" enctype="multipart/form-data">
                                <div class="col-md-6 form-group">
                                    <label for="inputIsValid" class=" form-control-label">Titere de la série</label>
                                    <input type="text" name="title" class="form-control">
                                    <span class="infoMessage"><?php echo form_error('title'); ?></span>
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="inputIsValid" class=" form-control-label">Date de sortie de la série</label>
                                    <input type="date" name="date" class="form-control">
                                    <span class="infoMessage"><?php echo form_error('date'); ?></span>
                                </div>


                                <div class="col-md-6 form-group">
                                    <label for="inputIsValid" class=" form-control-label">Catégorie de la série</label>
                                    <select name="type" id="type" class="form-control">
                                        <option value="" disabled selected>Choisissez un type de compte</option>
                                        <?php foreach ($typeComptes as $type):?>
                                            <option value="<?php echo $type->id_type;?>"><?php echo $type->nom_type;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="infoMessage"><?php echo form_error('type'); ?></span>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="inputIsValid" class=" form-control-label">Déscription de la série</label>
                                    <textarea name="description" id="textarea-input"  cols="0" rows="10" placeholder="Content..." class="form-control"></textarea>
                                    <span class="infoMessage"><?php echo form_error('description'); ?></span>
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="inputIsValid" class=" form-control-label">Lien vidéo</label>
                                    <input type="input" name="video" class="form-control">
                                    <span class="infoMessage"><?php echo form_error('video'); ?></span>
                                </div>

                                <div class="col-md-12 form-group">
                                    <label for="inputIsValid" class=" form-control-label">Importer l'image de la série</label>
                                    <input type="file" name="userfile" id="userfile">
                                </div>

                                <div class="col-md-4">
                                    <button class="btn btn-primary">
                                        Enregistrer
                                    </button>
                                </div>

                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
ajoutAnnonce
