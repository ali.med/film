<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des séries</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Titre </th>
                                        <th>Catégories</th>
                                        <th>Date de sortie</th>
                                        <th>Ajouté(e) par</th>
                                        <th>Ajouté(e) le</th>
                                        <th>Supprimer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($Series != FALSE):?>
                                    <?php foreach($Series as $serie):?>
                                        <tr>
                                            <td><?php echo $serie->titre_film;?></td>
                                            <th><?php echo $serie->nom_type;?></th>
                                            <th><?php echo date("d-m-Y", strtotime($serie->dateSortie));?></th>
                                            <td><?php echo $serie->login_admin;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($serie->date_add));?></td>
                                            <td><a href="<?php echo base_url();?>Admin/Series/SupprimerSeries/<?php echo $serie->id_film;?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    <?php endforeach;?>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>