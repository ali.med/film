<!-- ##### Hero Area Start ##### -->
<section class="hero--area section-padding-80">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-12 col-md-7 col-lg-8">
                <div class="tab-content">
                    <?php if($lastFilm != NULL):?>
                        <?php $active = 1;?>
                        <?php foreach ($lastFilm as $film):?>
                            <div class="tab-pane fade show <?php if ($active == 1):?>active<?php endif;?>" id="post-<?php echo $active;?>" role="tabpanel" aria-labelledby="post-<?php echo $active;?>-tab">
                                <!-- Single Feature Post -->
                                <div class="single-feature-post video-post bg-img" style="background-image: url(<?php echo base_url();?>uploads/image_film/<?php echo $film->image_film;?> ">
                                    <!-- Play Button 
                                    <a href="video-post.html" class="btn play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    -->
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="#" class="post-cata"><?php echo $film->nom_type;?></a>
                                        <a href="<?php echo base_url();?>Accueil/Description/<?php echo $film->id_film;?>" class="post-title"><?php echo $film->titre_film;?></a>
                                        <!--<div class="post-meta d-flex">
                                            <i classclass="fa fa-calendar" aria-hidden="true"> Date de sortie : <?php echo $film->dateSortie;?></i>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 25</a>
                                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 25</a>
                                            <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 25</a>
                                        </div>-->
                                    </div>

                                    <!-- Video Duration 
                                    <span class="video-duration">05.03</span>-->
                                </div>
                            </div>
                            <?php $active++;?>
                        <?php endforeach;?>
                    <?php else:?>
                    <?php endif;?>
                </div>
            </div>

            <div class="col-12 col-md-5 col-lg-4">
                <ul class="nav vizew-nav-tab" role="tablist">
                    <?php if($lastFilm != NULL):?>
                        <?php $active = 1;?>
                        <?php foreach ($lastFilm as $film):?>
                            <li class="nav-item" style="height: 0px;">
                                <a class="nav-link <?php if ($active == 1):?>active<?php endif;?>" id="post-<?php echo $active;?>-tab" data-toggle="pill" href="#post-<?php echo $active;?>" role="tab" aria-controls="post-<?php echo $active;?>" aria-selected="true">
                                    <!-- Single Blog Post -->
                                    <div class="single-blog-post style-2 d-flex align-items-center">
                                        <div class="post-thumbnail">
                                            <img src="<?php echo base_url();?>uploads/image_film/<?php echo $film->image_film;?>" alt="" style="width: 110px;height: 80px;">
                                        </div>
                                        <div class="post-content">
                                            <h6 class="post-title"><?php echo $film->titre_film;?></h6>
                                            <div class="post-meta d-flex justify-content-between">
                                                <span><i class="" aria-hidden="true"></i> <?php echo $film->dateSortie;?></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php $active++;?>
                        <?php endforeach;?>
                    <?php else:?>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- ##### Hero Area End ##### -->

<!-- ##### Trending Posts Area Start ##### -->
<section class="trending-posts-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading">
                        <h4>Les dernières films</h4>
                        <div class="line"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php if($lastFilm != NULL):?>
                    <?php foreach ($lastFilm as $film):?>
                        <!-- Single Blog Post -->
                        <div class="col-12 col-md-4">
                            <div class="single-post-area mb-80">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <img src="<?php echo base_url();?>uploads/image_film/<?php echo $film->image_film;?>" alt="">

                                    <!-- Video Duration -->
                                    <span class="video-duration"><?php echo $film->dateSortie;?></span>
                                </div>

                                <!-- Post Content -->
                                <div class="post-content">
                                    <a href="#" class="post-cata cata-sm cata-success"><?php echo $film->nom_type;?></a>
                                    <a href="<?php echo base_url();?>Accueil/Description/<?php echo $film->id_film;?>" class="post-title"><?php echo $film->titre_film;?></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                <?php else:?>
                <?php endif;?>
            </div>

        </div>
    </section>
<!-- ##### Trending Posts Area End ##### -->

<!-- ##### Trending Posts Area Start ##### -->
<section class="trending-posts-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading -->
                <div class="section-heading">
                    <h4>Les dernières séries</h4>
                    <div class="line"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php if($sixLastSeries != NULL):?>
                <?php foreach ($sixLastSeries as $series):?>
                    <!-- Single Blog Post -->
                    <div class="col-12 col-md-4">
                        <div class="single-post-area mb-80">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <img src="<?php echo base_url();?>uploads/image_serie/<?php echo $series->image_film;?>" alt="">

                                <!-- Video Duration -->
                                <span class="video-duration"><?php echo $series->dateSortie;?></span>
                            </div>

                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="#" class="post-cata cata-sm cata-success"><?php echo $series->nom_type;?></a>
                                <a href="<?php echo base_url();?>Accueil/Description/<?php echo $series->id_film;?>" class="post-title"><?php echo $series->titre_film;?></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php else:?>
            <?php endif;?>
        </div>

    </div>
</section>
<!-- ##### Trending Posts Area End ##### -->