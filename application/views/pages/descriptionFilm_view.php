   <!-- ##### Breadcrumb Area Start ##### -->
   <div class="vizew-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <?php foreach($detailFilm as $detail):?>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a></li>
                                <li class="breadcrumb-item"><a href="#"><?php echo $detail->nom_type;?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $detail->titre_film;?></li>
                            </ol>
                        <?php endforeach;?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <?php if($detailFilm != NULL):?>
        <!-- ##### Post Details Area Start ##### -->
        <section class="post-details-area mb-80">
          <?php foreach($detailFilm as $detail):?>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="single-video-area">
                                <iframe src="<?php echo $detail->lien_video;?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <!-- Post Details Content Area -->
                        <div class="col-md-12">
                            <div class="post-details-content d-flex">
                                <!-- Post Share Info -->
                                <div class="post-share-info">
                                    <p>Partager</p>
                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                </div>
                                <!-- Blog Content -->
                                <div class="blog-content">
                                    <!-- Post Content -->
                                    <div class="post-content mt-0">
                                        <a href="#" class="post-cata cata-sm cata-danger"><?php echo $detail->nom_type;?></a>
                                        <a href="single-post.html" class="post-title mb-2"><?php echo $detail->titre_film;?></a>

                                        <div class="d-flex justify-content-between mb-30">
                                            <div class="post-meta d-flex align-items-center">
                                                <a href="#" class="post-author">By Jane</a>
                                                <i class="fa fa-circle" aria-hidden="true"></i>
                                                <a href="#" class="post-date"><?php echo $detail->dateSortie;?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                        <?php echo $detail->description;?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </section>
        <!-- ##### Post Details Area End ##### -->
    <?php endif;?>
