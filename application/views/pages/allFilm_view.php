<!-- ##### Breadcrumb Area Start ##### -->
<div class="vizew-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Feature</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Archive by Category SPORTS</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcrumb Area End ##### -->

<!-- ##### Archive Grid Posts Area Start ##### -->
<div class="vizew-grid-posts-area mb-80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <!-- Archive Catagory & View Options -->
                <div class="archive-catagory-view mb-50 d-flex align-items-center justify-content-between">
                    <!-- Catagory -->
                    <div class="archive-catagory">
                        <h4><i class="fa fa-trophy" aria-hidden="true"></i> Sports</h4>
                    </div>
                    <!-- View Options -->
                    <div class="view-options">
                        <a href="archive-grid.html" class="active"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                        <a href="archive-list.html"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
                    </div>
                </div>

                <div class="row">
                    <!-- Single Blog Post -->
                    <?php if($results != FALSE):?>
                        <?php foreach($results as $film):?>
                            <div class="col-8 col-md-4">
                                <div class="single-post-area mb-50">
                                    <!-- Post Thumbnail -->
                                    <div class="post-thumbnail">
                                        <img src="<?php echo base_url();?>uploads/image_film/<?php echo $film->image_film;?>" alt="">

                                        <!-- Video Duration -->
                                        <span class="video-duration"><?php echo $film->dateSortie;?></span>
                                    </div>

                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="#" class="post-cata cata-sm cata-success"><?php echo $film->nom_type;?></a>
                                        <a href="<?php echo base_url();?>Accueil/Description/<?php echo $film->id_film;?>" class="post-title"><?php echo $film->titre_film;?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>

                <!-- Pagination -->
                <nav class="mt-50">
                    <ul class="pagination justify-content-center">
                        <?php foreach ($links as $link):?>
                            <li class="page-item"><?php echo $link;?></li>
                        <?php endforeach;?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ##### Archive Grid Posts Area End ##### -->