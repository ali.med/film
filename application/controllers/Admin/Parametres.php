<?php


class Parametres extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/Profile_model');
        $this->load->model('Admin/Plainte_model');
        $this->load->model('Admin/Login_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('id_user')) {
            redirect('Admin/Login');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $profileUser = $this->Profile_model->getProfileUser($id);
        $data['profileUser'] = $profileUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/Parametres_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function motPasse(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/ModificationMotPasse_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function modificationMotPasse()
    {
        $this->form_validation->set_rules('ancienpass', "ancien mot de passe", 'trim|required');
        $this->form_validation->set_rules('nouveaupass', 'nouveau mot de passe', 'trim|required');
        $this->form_validation->set_rules('cnfpass', 'confirmation du nouveau mot de passe', 'trim|required');

        if ($this->form_validation->run() == true)
        {
            $id = $this->session->userdata('id_user');
            $lastpassword = $this->Profile_model->getPassword($id);

            $ancienPass = md5($this->input->post('ancienpass'));
            $nouveaupass = md5($this->input->post('nouveaupass'));
            $cnfpass    = md5($this->input->post('cnfpass'));

            if($lastpassword == $ancienPass)
            {
                if($nouveaupass == $cnfpass)
                {
                    $data = array(
                        'password' => $nouveaupass
                    );

                    $nouveauPass = $this->Profile_model->changerMotPasse($id, $data);

                    if ($nouveauPass = true)
                    {
                        $action = "Modification mot de passe";
                        $this->histoirque($action);
                        redirect('Admin/Login/logout');
                    }
                    else{
                        $this->session->set_flashdata('error', 'Veuillez réessayer.');
                        redirect('Admin/Parametres/motPasse');
                    }
                }
                else{
                    $this->session->set_flashdata('error', 'Les deux mots de passe ne sont pas identiques.');
                    redirect('Admin/Parametres/motPasse');
                }
            }
            else{
                $this->session->set_flashdata('error', 'Votre ancien mot de passe est incorrect.');
                redirect('Admin/Parametres/motPasse');
            }
        }
        else{
            $this->motPasse();
        }
    }

    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_user'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }
}
