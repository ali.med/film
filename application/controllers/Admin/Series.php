<?php


class Series extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Login_model');
        $this->load->model('Admin/Series_model');
        $this->load->model('Admin/Admin_model');
        $this->load->model('Admin/TypeCompte_model');
        $this->load->model('Admin/Utilisateurs_model');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_admin'))
        {
            redirect('Admin/Login');
        }
        //if($this->session->userdata(''))
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }


    public function index()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $typeComptes = $this->TypeCompte_model->getAllType();
        $data['typeComptes'] = $typeComptes;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/ajoutSerie_view', $typeComptes);
        $this->load->view('admin/templates/footer');
    }

    public function addSerie(){
        $this->form_validation->set_rules('title', "Titre de la série", 'trim|required');
        $this->form_validation->set_rules('date', 'Date de sortie', 'trim|required');
        $this->form_validation->set_rules('type', 'Catégorie de la série', 'trim|required');
        $this->form_validation->set_rules('description', 'Déscription de la série', 'trim|required');

        if ($this->form_validation->run() == true) {
            //True

            //Config image
            $config['upload_path'] = './uploads/image_serie';
            $config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
            $config['max_size'] = 1024;
            $config['max_width'] = 1924;
            $config['max_height'] = 1924;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('userfile')) {
                $id_admin = $this->session->userdata('id_admin');

                $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
                $data['infoUser'] = $infoUser;

                $typeComptes = $this->TypeCompte_model->getAllType();
                $data['typeComptes'] = $typeComptes;

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error',$error['error']);

                $this->load->view('admin/templates/header', $data);
                $this->load->view('admin/pages/ajoutSerie_view', $data);
                $this->load->view('admin/templates/footer');
            }
            else{
                $full_path = $this->upload->data('file_name');
                $title = $this->input->post('title');
                $date_sortie = $this->input->post('date');
                $type = $this->input->post('type');
                $description = $this->input->post('description');
                $video = $this->input->post('video');
                $id_admin = $this->session->userdata('id_admin');
                $date_add = $this->getDatetimeNow();

                $data = array(
                    'titre_film'        => $title,
                    'id_cate'           => $type,
                    'dateSortie'        => $date_sortie,
                    'description '      => $description,
                    'image_film'        => strtolower($full_path),
                    'lien_video'        => $video,
                    'id_admin_add'      => $id_admin,
                    'date_add'          =>$date_add
                );

                $addSerie = $this->Series_model->addSerie($data);

                if ($addSerie = true)
                {
                    $action = "Ajout d'une série";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Ajout série réussi');
                    redirect('Admin/Series/listsSeries');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Series/');
                }

            }
        }
        else{
            $this->index();
        }
    }

    /** Liste des films */
    public function listsSeries(){
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $Series = $this->Series_model->getAllSeries();
        $data['Series'] = $Series;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listeSerie_view', $data);
        $this->load->view('admin/templates/footer');
    }

    /**Supprimer un film */
    public function SupprimerSeries($id)
    {
        $data = array(
            'id_admin_delete' => $this->session->userdata('id_admin'),
            'date_delete' => $this->getDatetimeNow()
        );

        $deleteSupprimer= $this->Series_model->suppressionSerie($id, $data);

        if ($deleteSupprimer = true)
        {
            $action = "Suppression de la série";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression série réussi');
            redirect('Admin/Series/listsSeries');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Series/listsSeries');
        }
    }

    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_admin'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }



}