<?php


class Administrateur extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Login_model');
        $this->load->model('Admin/Admin_model');
        $this->load->model('Admin/TypeCompte_model');
        $this->load->model('Admin/Utilisateurs_model');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_admin'))
        {
            redirect('Admin/LoginAdmin');
        }
        //if($this->session->userdata(''))
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }


    public function index()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $Users = $this->Utilisateurs_model->getAllUser();
        $data['Users'] = $Users;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listUtilisateurs_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function ajoutUtilisateur()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $typeComptes = $this->TypeCompte_model->getAllType();
        $data['typeComptes'] = $typeComptes;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/ajoutUtilisateur_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function VerifyForm()
    {
        $this->form_validation->set_rules('username', "nom d'utilisateur", 'trim|required');
        $this->form_validation->set_rules('password', 'mot de passe ', 'trim|required');
        $this->form_validation->set_rules('cnfpassword', 'confirmation du mot de passe', 'trim|required');
        $this->form_validation->set_rules('email', 'email ', 'trim|required|valid_email|is_unique[utilisateurs.email]');
        $this->form_validation->set_rules('type', 'type', 'trim|required');

        if ($this->form_validation->run() == true)
        {
            //True
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $cnfpass  = md5($this->input->post('cnfpassword'));
            $email    = $this->input->post('email');
            $type     = $this->input->post('type');
            $id_admin = $this->session->userdata('id_admin');
            $date_add = $this->getDatetimeNow();

            if($password == $cnfpass)
            {
                $data = array(
                    'username' => $username,
                    'password' => $password,
                    'email '   => $email,
                    'id_type'  => $type,
                    'id_admin_add' => $id_admin,
                    'date_add' => $date_add
                );

                $addUser = $this->Utilisateurs_model->addUser($data);

                if ($addUser = true)
                {
                    $action = "Ajout utilisateur";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Ajout utilisateur réussi');
                    redirect('Admin/Administrateur');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Administrateur/ajoutUtilisateur');
                }

            }
            else{
                $this->session->set_flashdata('error', 'Les deux mots de passes ne sont pas identités.');
                redirect('Admin/Administrateur/ajoutUtilisateur');
            }
        }
        else{
            $this->ajoutUtilisateur();
        }
    }

    public function SupprimerUtilisateur($id)
    {
        $data = array(
            'id_admin_delete' => $this->session->userdata('id_admin'),
            'date_delete' => $this->getDatetimeNow()
        );

        $deleteUser = $this->Utilisateurs_model->suppressionUtilisateur($id, $data);

        if ($deleteUser = true)
        {
            $action = "Suppression d'un utilisateur";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression utilisateur réussi');
            redirect('Admin/Administrateur/');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Administrateur/');
        }
    }
    /** Type Compte**/

    public function ListeTypeCompte()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $typeComptes = $this->TypeCompte_model->getAllType();
        $data['typeComptes'] = $typeComptes;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listTypeCompte_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function ajoutTypeCompte()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/ajoutTypeCompte_view');
        $this->load->view('admin/templates/footer');
    }

    public function VerifyFormType()
    {
        $this->form_validation->set_rules('type', "libellé type", 'trim|required');

        if ($this->form_validation->run() == true) {
            //True
            $type = $this->input->post('type');

            $data = array(
                'nom_type' => $type,
                'id_admin_add_type' => $this->session->userdata('id_admin'),
                'date_add_type' => $this->getDatetimeNow()
            );

            $addTypeCompte = $this->TypeCompte_model->addTypeCompte($data);

            if ($addTypeCompte = true)
            {
                $action = "Ajout type compte";
                $this->histoirque($action);
                $this->session->set_flashdata('sucess', 'Ajout type compte réussi');
                redirect('Admin/Administrateur/ListeTypeCompte');
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/Administrateur/ajoutTypeCompte');
            }
        }
        else{
            $this->ajoutTypeCompte();
        }
    }
    public function SupprimerTypeCompte($id)
    {
        $data = array(
            'id_admin_delete_type' => $this->session->userdata('id_admin'),
            'date_delete_type' => $this->getDatetimeNow()
        );

        $deleteTypeCompte = $this->TypeCompte_model->suppressionTypeCompte($id, $data);

        if ($deleteTypeCompte = true)
        {
            $action = "Suppression type de compte";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression type de compte réussi');
            redirect('Admin/Administrateur/ListeTypeCompte');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Administrateur/ListeTypeCompte');
        }
    }
    public function ajoutFilm()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Admin_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $typeComptes = $this->TypeCompte_model->getAllType();
        $data['typeComptes'] = $typeComptes;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/ajoutFilm_view', $typeComptes);
        $this->load->view('admin/templates/footer');
    }

    public function addFilm(){
        $this->form_validation->set_rules('title', "Titre du film", 'trim|required');
        $this->form_validation->set_rules('date', 'Date de sortie', 'trim|required');
        $this->form_validation->set_rules('type', 'Catégorie du film', 'trim|required');
        $this->form_validation->set_rules('description', 'Déscription du film', 'trim|required');

        if ($this->form_validation->run() == true) {
            //True
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $cnfpass = md5($this->input->post('cnfpassword'));
            $email = $this->input->post('email');
            $type = $this->input->post('type');
            $id_admin = $this->session->userdata('id_admin');
            $date_add = $this->getDatetimeNow();
        }
        else{
            $this->ajoutFilm();
        }
    }

    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_admin'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }



}