<?php
/**
 *
 */
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Login_model');
        $this->load->model('Admin/Profile_model');
        $this->load->library('form_validation');
        $this->load->helper('string');
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $this->load->view('admin/Login_view');
    }

    public function verifylogin()
    {
        $this->form_validation->set_rules('username', "Nom d'utilisateur", 'trim|required');
        $this->form_validation->set_rules('pass', 'Password ', 'trim|required');

        if($this->form_validation->run()==true)
        {
            //true
            $username = $this->input->post('username');
            $password = md5($this->input->post('pass'));

            //model function
            if($result = $this->Login_model->can_login($username, $password))
            {
                //var_dump($result);die;
                if($result)
                {
                    $session_data = array(
                        'id_admin' => $result[0]->id_admin,
                        'email_admin' => $result[0]->email_admin,
                        'login_admin' =>$result[0]->login_admin
                    );
                }
                $this->session->set_userdata($session_data);
                $action = "Connexion";
                $this->histoirque($action);
                redirect("Admin/Administrateur");
            }
            else
            {
                $data['error_message'] = 'Nom d\'utilisateur ou mot de passe incorrect';
                $this->load->view('admin/Login_view', $data);
            }

        }
        else
        {
            //false
            $this->index();
        }
    }

    //Logout function
    public function logout()
    {
        $action = "Déconnexion";
        $this->histoirque($action);
        $this->session->unset_userdata('id_admin');
        session_destroy();
        redirect('Admin/Login', 'refresh');
    }
    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_admin'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }
}

?>
