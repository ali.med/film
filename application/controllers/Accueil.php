<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Film_model');
        $this->load->model('Series_model');
        $this->load->model('Home_model');
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $lastFilm = $this->Film_model->getLastFilm();
        $data['lastFilm'] = $lastFilm;

        /** Catégorie*/
        $categories = $this->Home_model->getAllCategories();
        $data['categories'] = $categories;

        /** Get 6 films */

        $sixLastFilm = $this->Film_model->getSixLastFilm();
        $data['sixLastFilm'] = $sixLastFilm;

        /** Get 6 series */

        $sixLastSeries = $this->Series_model->getSixLastSeries();
        $data['sixLastSeries'] = $sixLastSeries;

        $this->load->view('templates/header', $data);
        $this->load->view('pages/index_view', $data);
        $this->load->view('templates/footer');
    }

    public function Description($id)
    {
        /** Catégorie*/
        $categories = $this->Home_model->getAllCategories();
        $data['categories'] = $categories;

        /** Detail film*/
        $detailFilm = $this->Film_model->getDetailtFilm($id);
        $data['detailFilm'] = $detailFilm;

        $this->load->view('templates/header');
        $this->load->view('pages/descriptionFilm_view', $data);
        $this->load->view('templates/footer');
    }
}
