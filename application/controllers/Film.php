<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Film extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Film_model');
        $this->load->model('Home_model');
        $this->load->library('pagination');
    }
    public function Categories($id)
    {
        $row =  $this->Film_model->record_count($id);
        //var_dump($row);die();
        $config = array();

        $config["base_url"] = base_url() . "Film/Categories/";

        $config["total_rows"] = $row;

        $config["per_page"] = 1;

        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(3) : 0;
        /** Categories films */
        $results = $this->Film_model->fetch_article($config["per_page"], $page, $id);
        $data['results'] = $results;

        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;',$str_links );



        /** Catégorie*/
        $categories = $this->Home_model->getAllCategories();
        $data['categories'] = $categories;


        $this->load->view('templates/header', $data);
        $this->load->view('pages/allFilm_view', $data);
        $this->load->view('templates/footer');
    }

}
